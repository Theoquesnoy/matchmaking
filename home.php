<html>

<head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="home.php">Matchmaking</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="create_user.php">Créer son compte</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="login.php">Se connecter</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="profil.php">Profil</a>
                </li>
            </ul>
        </div>
    </nav>
</body>

</html>

<?php
session_start();
$mysqli = new mysqli("localhost", "root", "", "matchmaking");
if (!$mysqli) {
    die("Échec de la connexion : " . mysqli_connect_error());
}

if(@$_SESSION['login'] == "") {
    ?><br><h1 class="text-center mb-1"> <?php echo("Veuillez vous connectez"); ?> </h1>
<?php
} else {
$pseudo = $_SESSION['login'];
?><br><h1 class="text-center mb-1"> <?php echo("Bonjour $pseudo !"); ?> </h1>
<?php
}

?><br><h1 class="text-center mb-2"> <?php echo("Bienvenue sur la page d'accueil !\n"); ?> </h1>
<?php
if (isset($_POST['result'])) {
    header("Location: ./results.php");
}

?>

<?php mysqli_close($mysqli); ?>

<body>
<br>
    <br>
    <?php
    if(@$_SESSION['login'] != ""){ ?>
    <form method="POST" action="home.php">
        <div class="text-center mb-3"><button class="w-30 btn btn-lg btn-primary" type="submit" name="result">Donner les résultats de votre partie !</button></div>
    </form>
    <?php } ?>

    <br>
    <br>
</body>