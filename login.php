<?php
$mysqli = new mysqli("localhost", "root", "", "matchmaking");
if (!$mysqli) {
    die("Échec de la connexion : " . mysqli_connect_error());
}

?>
<?php
if (isset($_POST['login'])) {

    $username = $_POST['username'];
    $password = $_POST['password'];

    $query = mysqli_query($mysqli, "select * from `users` where Pseudo='$username' && MDP='$password'");

    if (mysqli_num_rows($query) == 0) {
        ?> <div class="text-center mb-4" <?php $_SESSION['message'] = "Login Failed. No user Found!"; ?>>
    <?php } else {
        $row = mysqli_fetch_array($query);

        $_SESSION['id'] = $row['Id'];
        session_start();
        $_SESSION['login'] = $username;
        echo "Bonjour " . $_SESSION['login']; 
        header("Location: ./home.php")?>
        <br>
        <br>
    <?php }
} else {
    ?> <div class="text-center mb-4" <?php $_SESSION['message'] = "Connectez-vous"; ?>>
<?php } ?>
<html>

<head>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="home.php">Matchmaking</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="create_user.php">Créer son compte</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="login.php">Se connecter</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="profil.php">Profil</a>
                </li>
            </ul>
        </div>
    </nav>
    <br>
    <br>

    <h1 class="h3 mb-3 fw-normal">Please login</h1>
    <form method="POST" action="login.php">
        <div class="text-center mb-1" ><input type="text" name="username" placeholder="Username"></div>
        <div class="text-center mb-2"><input type="password" name="password" placeholder="Mot de passe"></div>
        <div class="text-center mb-3"><button class="w-30 btn btn-lg btn-primary" type="submit" value="Login" name="login">Login</button></div>
    </form>

    <span>
        <?php
        if (isset($_SESSION['message'])) {
            echo $_SESSION['message'];
        }
        unset($_SESSION['message']);
        ?>

    </span>
    <br>
    <br>
    <div class="text-center mb-5"><a href="/matchmaking/create_user.php">Pas de compte ? Créer un compte maintenant !</a></div>
</body>

</html>