from socket import AF_INET, socket, SOCK_STREAM
from threading import Thread
from signal import signal, SIGINT
import sys
import subprocess

def receive_msg():
    while True:
        try:
            msg = client_socket.recv(BUFFERSIZE).decode("utf8")
            print(msg)
        except OSError as error:
            return error

def send_msg():
    while True:
        try:
            msg = input()
            if msg != 'exit()':
                client_socket.send(msg.encode('utf8'))
                if msg == 'jouer()':
                    print("jeu lancé")
                    subprocess.call("python démineur/main.py 15 15 0.1", shell=True)
                    
            else:
                clean_exit()
        except EOFError:
            clean_exit()

def clean_exit():
    client_socket.send('exit()'.encode('utf8'))
    client_socket.close()
    sys.exit(0)

def handler(signal_recv, frame):
    clean_exit()

if __name__ == '__main__':
    signal(SIGINT, handler)
    HOST = str(sys.argv[1])
    PORT = 24580
    BUFFERSIZE = 1024
    ADDR = (HOST, PORT)
    client_socket = socket(AF_INET, SOCK_STREAM)
    client_socket.connect(ADDR)
    receive_thread = Thread(target=receive_msg)
    receive_thread.start()
    send_msg()  