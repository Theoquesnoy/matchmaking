# Matchmaking

Faire un site avec du matchmaking, création + gestion du compte, choix du jeu et attente d'un joueur.

Site + serveur de matchmaking :

- PHP HTML/CSS
- Socket pour multi : difficulté 10
- Lien avec base de données (pseudo, xp, ...) en MYSQL : difficulté 3
- Intéraction avec les différentes pages du site : difficulté 2
- Choix du jeu : difficulté 5
- Chat entre les 2 joueurs : difficulté 1

Gestion de compte :

- MYSQL
- création d'un compte (pseudo, adresse mail, mdp, ...)
- Modifier ses données, supprimer compte

Jeu :

- Choisir un jeu et attendre quelqu'un : difficulté 2

Sur le site, il faudra être connecté pour pouvoir sélectionner un jeu. Lors de la création du compte, renseignez pseudo, mot de passe, adresse mail. (Peut-être si pseudo deja pris modifier le pseudo).
Une fois connecté, retour sur la page d'accueil où l'on pourra choisir un jeu au choix. Une fois le jeu choisi, on est redigiré sur une page d'attente en attendant qu'un joueur se connecte aussi. Quand un autre rejoint, le jeu se lancera et le joueur qui finit en 1er gagne avec un message qui s'affiche et la connexion se coupe.

Etapes à réaliser pour le bon fonctionnement du projet :

- Cloner le git
- Installer python et pygame `pip install pygame`
- Installer xampp accepter les accès pare-feu
- Copier les fichiers du repertoire git dans le dossier C:\\xamp\\htdocs\\matchmaking (dossier matchmaking a créer)
- Lancer le serveur apache et mysql accepter les accès pare-feu
- Aller sur phpmyadmin (localhost/phpmyadmin)
- Créer la base de données `matchmaking` et importer la base de donnée matchmaking.sql

Vous pouvez maintenant aller sur le site `localhost/matchmaking/home.php`

Pour pouvoir jouer en local et renseigner le vainqueur :

- Un des deux jouers lance le serveur qui se trouve dans le dossier matchmaking de xampp (C:\\xamp\\htdocs\\matchmaking) sur son pc, avec cette commande `python .\server.py <votre_adresse_ip>`, accepter le pare-feu.
- Lancez ensuite chacun de votre côté client.py `python .\client.py adresse ip_du_serveur`, qui se trouve au même endroit que le serveur.

Vous pourrez alors changer de nom, discutez dans le tchat, lancer le jeu.

Une fois prêt, lancer le jeu et le 1er qui fini ou qui joue le plus longtemps à gagné !

N'oubliez de préciser dans le tchat lorsque vous avez perdu ou gagné.
