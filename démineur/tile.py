class Tile:
    def __init__(self, hasBomb):
        self.hasBomb = hasBomb
        self.aroundBombs = 0
        self.clicked = False
        self.flagged = False
        self.tiles = []

    def __str__(self):
        return str(self.hasBomb)

    def getbombsAround(self):
        return self.aroundBombs

    def getHasBomb(self):
        return self.hasBomb

    def getClick(self):
        return self.clicked

    def getFlagged(self):
        return self.flagged

    def toggleFlag(self):
        self.flagged = not self.flagged

    def Click(self):
        self.clicked = True

    def setbombsAround(self):
        num = 0
        for tile in self.tiles:
            if tile.getHasBomb():
                num += 1
        self.aroundBombs = num

    def setAdjacentTiles(self, tiles):
        self.tiles = tiles
        
    def getAdjacentTiles(self):
        return self.tiles