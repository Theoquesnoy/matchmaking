import random
from tile import Tile
class boardGame:
    def __init__(self, size, prob):
        self.size = size
        self.board = []
        self.win = False 
        self.lose = False
        for rows in range(size[0]):
            rows = []
            for cols in range(size[1]):
                bombs = random.random() < prob
                tile = Tile(bombs)
                rows.append(tile)
            self.board.append(rows)
        self.setAdjacentTiles()
        self.setbombsAround()

    def print(self):
        for rows in self.board:
            for tile in rows:
                print(tile, end=" ")
            print()

    def getBoard(self):
        return self.board

    def getSize(self):
        return self.size
    
    def getTile(self, index):
        return self.board[index[0]][index[1]]

    def Click(self, tile, flag):
        if tile.getClick() or (tile.getFlagged() and not flag):
            return
        if flag:
            tile.toggleFlag()
            return
        tile.Click()
        if tile.getbombsAround() == 0:
            for tile in tile.getAdjacentTiles():
                self.Click(tile, False)
        if tile.getHasBomb():
            self.lose = True
        else:
            self.win = self.checkWin()
    
    def checkWin(self):
        for rows in self.board:
            for tile in rows:
                if not tile.getHasBomb() and not tile.getClick():
                    return False
        return True

    def getWin(self):
        return self.win

    def getLose(self):
        return self.lose

    def setAdjacentTiles(self):
        for rows in range(len(self.board)):
            for cols in range(len(self.board[0])):
                tile = self.board[rows][cols]
                tiles = []
                self.addToAdjacentTilesList(tiles, rows, cols)
                tile.setAdjacentTiles(tiles)
    
    def addToAdjacentTilesList(self, tiles, rows, cols):
        for r in range(rows - 1, rows + 2):
            for c in range(cols - 1, cols + 2):
                if r == rows and c == cols:
                    continue
                if r < 0 or r >= self.size[0] or c < 0 or c >= self.size[1]:
                    continue
                tiles.append(self.board[r][c])
    
    def setbombsAround(self):
        for rows in self.board:
            for tile in rows:
                tile.setbombsAround()