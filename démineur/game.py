import pygame
from boardGame import boardGame 
import os
from time import sleep

class Game:
    def __init__(self, size, prob):
        self.board = boardGame(size, prob)
        pygame.init()
        self.screenSize = 800, 800
        self.screenGame = pygame.display.set_mode(self.screenSize)
        self.tileSize = (self.screenSize[0] / size[1], self.screenSize[1] / size[0]) 
        self.loadPictures()

    def loadPictures(self):
        self.images = {}
        imagesDirectory = "démineur/images"
        for fileName in os.listdir(imagesDirectory):
            path = imagesDirectory + r"/" + fileName 
            img = pygame.image.load(path)
            img = img.convert()
            img = pygame.transform.scale(img, (int(self.tileSize[0]), int(self.tileSize[1])))
            self.images[fileName.split(".")[0]] = img
            
    def runGame(self):
        running = True
        while running:
            for e in pygame.event.get():
                if e.type == pygame.QUIT:
                    running = False
                if e.type == pygame.MOUSEBUTTONDOWN and not (self.board.getWin() or self.board.getLose()):
                    rightClick = pygame.mouse.get_pressed(num_buttons=3)[2]
                    self.Click(pygame.mouse.get_pos(), rightClick)
            self.screenGame.fill((0, 0, 0))
            self.drawBoard()
            pygame.display.flip()
            if self.board.getWin():
                self.Win()
                running = False
        pygame.quit()
        
    def drawBoard(self):
        leftCorner = (0, 0)
        for rows in self.board.getBoard():
            for tile in rows:
                image = self.images[self.getPictureString(tile)]
                self.screenGame.blit(image, leftCorner) 
                leftCorner = leftCorner[0] + self.tileSize[0], leftCorner[1]
            leftCorner = (0, leftCorner[1] + self.tileSize[1])

    def getPictureString(self, tile):
        if tile.getClick():
            return str(tile.getbombsAround()) if not tile.getHasBomb() else 'bomb_explosed_tile'
        if (self.board.getLose()):
            if (tile.getHasBomb()):
                return 'bombs_revealed_tile'
            return 'wrong_flag_tile' if tile.getFlagged() else 'game_tile'
        return 'flag_tile' if tile.getFlagged() else 'game_tile'

    def Click(self, position, flag):
        index = tuple(int(pos // size) for pos, size in zip(position, self.tileSize))[::-1] 
        self.board.Click(self.board.getTile(index), flag)

    def Win(self):
        sleep(3)