<?php

$mysqli = new mysqli("localhost", "root", "", "matchmaking");
if (!$mysqli) {
    die("Échec de la connexion : " . mysqli_connect_error());
}

?>
<html>

<head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="home.php">Matchmaking</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="create_user.php">Créer son compte</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="login.php">Se connecter</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="profil.php">Profil</a>
                </li>
            </ul>
        </div>
    </nav>
    <br>

    <h1 class="text-center mb-1; h3 mb-3 fw-normal">Register</h1>
    <form name="formulaire" action="create_user.php" method="post">
        <div class="text-center mb-1"><input name="pseudo" placeholder="Pseudo"></textarea></div>
        <div class="text-center mb-2"><input type="password" name="Password" placeholder="Mot de passe"></div>
        <div class="text-center mb-3"><input type="email" name="EmailAddress" placeholder="Adresse Mail" style="width: 250px"></textarea></div>
        <div>
            <p class="text-center mb-3"><button name="submitDB" class="btn btn-primary"><span>Créer son compte !</span></button></p>
        </div>
    </form>
</body>

</html>

<?php

@$pseudo = $_POST['pseudo'];
@$mdp = $_POST['Password'];
@$mailAdress = $_POST['EmailAddress'];

if (isset($_POST['submitDB'])) {
    $check = $mysqli->query("SELECT Pseudo FROM users WHERE Pseudo = '$pseudo'");
    if ($check->num_rows == 0) {
        $result = $mysqli->query("INSERT INTO users (Pseudo, MDP, Adresse_Mail) VALUES ('$pseudo', '$mdp', '$mailAdress')");
        ?> <div class="text-center mb-3"><?php echo "Votre compte à été créé !";?></div> <?php
        $result = $mysqli->query("INSERT INTO statistiques (Pseudo, Experience, PartiesJouees, PartiesGagnes) VALUES ('$pseudo',0,0,0)");
    } else {
        echo "Le pseudo est déjà pris. Veuillez en choisir un autre";
    }
}

mysqli_close($mysqli);
?>