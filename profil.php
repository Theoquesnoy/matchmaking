<?php
session_start();
if(@$_SESSION['login'] == "") {
    header("Location: ./home.php");
}
$log = $_SESSION['login'];
$mysqli = new mysqli("localhost", "root", "", "matchmaking");
if (!$mysqli) {
    die("Échec de la connexion : " . mysqli_connect_error());
}
$ad = $mysqli->query("SELECT Adresse_Mail FROM users WHERE Pseudo = '$log'");
$dataAd = mysqli_fetch_array($ad);
$played = $mysqli->query("SELECT PartiesJouees FROM statistiques WHERE Pseudo = '$log'");
$dataPlayed = mysqli_fetch_array($played);
$won = $mysqli->query("SELECT PartiesGagnes FROM statistiques WHERE Pseudo = '$log'");
$dataWon = mysqli_fetch_array($won);
$xp = $mysqli->query("SELECT Experience FROM statistiques WHERE Pseudo = '$log'");
$dataXp = mysqli_fetch_array($xp);
?>

<html>

<head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="home.php">Matchmaking</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="create_user.php">Créer son compte</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="login.php">Se connecter</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="profil.php">Profil</a>
                </li>
            </ul>
        </div>
    </nav>
    <br>
    <h1 class="h3 mb-3 fw-normal; text-center mb-1">Modifications</h1>

    <form name="formulaire" action="profil.php" method="post">
        <div class="text-center mb-1"><input name="pseudo" placeholder="Pseudo"></div>
        <div class="text-center mb-2"><input type="password" name="Password" placeholder="Mot de passe"></div>
        <div class="text-center mb-3"><input type="email" name="EmailAddress" placeholder="Adresse Mail" style="width: 250px"></textarea></div>
        <div>
            <p class="text-center mb-3"><button name="ps" class="btn btn-primary"><span>Modifier mon pseudo</span></button></p>
            <p class="text-center mb-3"><button name="mdp" class="btn btn-primary"><span>Modifier mon mot de passe</span></button></p>
            <p class="text-center mb-3"><button name="ad" class="btn btn-primary"><span>Modifier mon adresse mail</span></button></p>
            <p class="text-center mb-3"><button name="delete" class="btn btn-danger"><span>Supprimer mon compte</span></button></p>
        </div>
    </form>
    <br>
    <br>
    <p class="text-center mb-3">Addresse Mail : <?php echo $dataAd['Adresse_Mail'];?> </p>
    <p class="text-center mb-3">Nombre de parties jouées : <?php echo $dataPlayed['PartiesJouees'];?> </p>
    <p class="text-center mb-3">Nombre de parties gagnées : <?php echo $dataWon['PartiesGagnes'];?></p>
    <p class="text-center mb-3">Expérience accumulée : <?php echo $dataXp['Experience'];?></p>

</body>

</html>

<?php

@$pseudo = $_POST['pseudo'];
@$mdp = $_POST['Password'];
@$mailAdress = $_POST['EmailAddress'];

if (isset($_POST['ad'])) {
    $check = $mysqli->query("SELECT * FROM users WHERE Pseudo = '$log'");
    if ($check->num_rows == 1) {
        $result = $mysqli->query("UPDATE users SET Adresse_Mail='$mailAdress' WHERE Pseudo='$log'");
        echo "Adresse mail modifiée !";
        header("Location: ./profil.php");
    }
}
if (isset($_POST['mdp'])) {
    $check = $mysqli->query("SELECT * FROM users WHERE Pseudo = '$log'");
    if ($check->num_rows == 1) {
        $result = $mysqli->query("UPDATE users SET MDP='$mdp' WHERE Pseudo='$log'");
        ?><p class="text-center mb-3"><?php echo "Mot de passe modifié !";?></p><?php 
    }
}
if (isset($_POST['ps'])) {
    $check = $mysqli->query("SELECT * FROM users WHERE Pseudo = '$log'");
    if ($check->num_rows == 1) {
        $result = $mysqli->query("UPDATE users SET Pseudo='$pseudo' WHERE Pseudo='$log'");
        header("Location: ./login.php");
    }else{
        echo "Pseudo déjà pris. Veuillez en choisir un autre.";
    }
}
if (isset($_POST['delete'])) {
    $check = $mysqli->query("SELECT * FROM users WHERE Pseudo = '$log'");
    if ($check->num_rows == 1) {
        $result = $mysqli->query("DELETE FROM users WHERE Pseudo='$log'");
        header("Location: ./create_user.php");
    }
}


mysqli_close($mysqli);
?>