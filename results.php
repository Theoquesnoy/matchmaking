<?php
session_start();
$log = $_SESSION['login'];
$mysqli = new mysqli("localhost", "root", "", "matchmaking");
if (!$mysqli) {
    die("Échec de la connexion : " . mysqli_connect_error());
}

?>

<html>

<head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="home.php">Matchmaking</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="create_user.php">Créer son compte</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="login.php">Se connecter</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="profil.php">Profil</a>
                </li>
            </ul>
        </div>
    </nav>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <form name="formulaire" action="results.php" method="post">
        <div class="row justify-content-center">    
            <div class="col-4">
                 <button class="btn btn-success btn-square-md" type="submit" value="Win" name="win" style="height:450px; width: 450px; font-size : 150px;">Win</button>
            </div>
            <div class="col-4">
                <button class="btn btn-danger btn-square-md" type="submit" value="Lose" name="lose" style="height:450px; width: 450px; font-size : 150px;">Lose</button>
            </div>
        </form>
    </div>
    

    
</body>

</html>

<?php 

if (isset($_POST['win'])) {
    $result = $mysqli->query("UPDATE statistiques SET Experience= Experience + 35, PartiesGagnes = PartiesGagnes + 1, PartiesJouees = PartiesJouees + 1 WHERE Pseudo='$log'");
}

if (isset($_POST['lose'])) {
    $result = $mysqli->query("UPDATE statistiques SET Experience= Experience + 10, PartiesJouees = PartiesJouees + 1 WHERE Pseudo='$log'");
}

?>

